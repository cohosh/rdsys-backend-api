# rdsys backend API

### Usage

```rust
use rdsys_backend::start_stream;
use tokio;

#[tokio::main]
async fn main() {
    let endpoint = String::from("http://127.0.0.1:7100/resource-stream");
    let name = String::from("https");
    let token = String::from("HttpsApiTokenPlaceholder");
    let types = vec![String::from("obfs2"), String::from("scramblesuit")];
    let rx = start_stream(endpoint, name, token, types).await.unwrap();
    for diff in rx {
        println!("Received diff: {:?}", diff);
    }
}
```
